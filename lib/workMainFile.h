#ifndef WORKMAINFILE_H
#define WORKMAINFILE_H

#include <iostream>
#include <string>
#include <memory>

class XML_IMStartFile{
	public:
		std::string root = "INDICATOR_MARK_START_FILE",
			msh_set_fn = "MESH_SET",
			geom_set_fn = "GEOM_SET",
			global_prm_fn = "GLOBAL_PARAMS",
			geolog_fn = "GEOLOG",
			save_dir = "SAVE_DIR",
			ofp_fn = "OFP",
			bound_fn_set = "BOUND_SET",
			res_sat_set = "RES_SATUR_SOLVER",
			fracts_sat_set = "FRACTS_SATUR_SOLVER";


		XML_IMStartFile();
		~XML_IMStartFile();

};

class IMStartFile{
	public:
		std::string save_dir_main;
		std::string mesh_set_fn;
		std::string ofp_fn;
		std::string geolog_fn;
		std::string global_params_fn;
		std::string geom_set_fn;
		std::string bound_fn_set;
		std::string res_sat_set_fn;
		std::string fracts_sat_set_fn;

		void read_start_file (const std::string &fn);

		IMStartFile();
		~IMStartFile();
};

#endif
