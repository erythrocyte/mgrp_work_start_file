set(HEADERS 
	indicatorMark.h
	workMainFile.h
	workBoundSetSimpleFile.h
)

set (SOURCES
	indicatorMark.cpp
	workMainFile.cpp
	workBoundSetSimpleFile.cpp
)


source_group ("Header Files" FILES ${HEADERS})
source_group ("Source Files" FILES ${SOURCES})

add_library(${LIB_INDICATOR_MARK_TARGET} SHARED ${HEADERS} ${SOURCES})

#set_target_properties(${SONAME} PROPERTIES 
	#VERSION ${MGRP_INDICATOR_MARK_VERSION}
	#)

# c++11 standart
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall") 

include_directories(${CMAKE_SOURCE_DIR}/src/lib_common_classes)

install(TARGETS ${LIB_INDICATOR_MARK_TARGET}
	ARCHIVE DESTINATION ${LIB_INSTALL_DIR}
	LIBRARY DESTINATION ${LIB_INSTALL_DIR}
)

include_directories(${CMAKE_SOURCE_DIR}/src/lib_pressure_solver)
include_directories(${CMAKE_SOURCE_DIR}/src/lib_satur_reservoir_solver)
include_directories(${CMAKE_SOURCE_DIR}/src/lib_satur_fract_solver)
include_directories(${CMAKE_SOURCE_DIR}/src/lib_conc_reservoir_solver)
include_directories(${CMAKE_SOURCE_DIR}/src/lib_conc_fract_solver)
include_directories(${CMAKE_SOURCE_DIR}/src/lib_work_string)
include_directories(${CMAKE_SOURCE_DIR}/src/lib_work_vector)
include_directories(${CMAKE_SOURCE_DIR}/src/lib_work_xml)
include_directories(${CMAKE_SOURCE_DIR}/src/lib_common_classes)
include_directories(${CMAKE_SOURCE_DIR}/src/lib_work_file)
include_directories(${CMAKE_SOURCE_DIR}/src/lib_well_task)
