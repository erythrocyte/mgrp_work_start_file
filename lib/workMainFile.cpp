#include "workMainFile.h"

#include "workXml.h"


XML_IMStartFile::XML_IMStartFile(){}
XML_IMStartFile::~XML_IMStartFile(){}

void IMStartFile::read_start_file (const std::string &fn)
{
	std::shared_ptr<XML_IMStartFile> xml_isf (new XML_IMStartFile());
	//std::shared_ptr<HeaderSaturStartFile> hssf (new HeaderSaturStartFile());
	std::shared_ptr<XmlReadWrite> xml (new XmlReadWrite(fn));

	xml->Get_Doc();
	this->save_dir_main = xml->getOneNodeValString(xml->rootNode, 
			xml_isf->save_dir);
	this->mesh_set_fn = xml->getOneNodeValString(xml->rootNode, 
			xml_isf->msh_set_fn);
	this->ofp_fn = xml->getOneNodeValString(xml->rootNode, xml_isf->ofp_fn);
	this->geolog_fn = xml->getOneNodeValString(xml->rootNode, xml_isf->geolog_fn);
	this->geom_set_fn = xml->getOneNodeValString(xml->rootNode,
			xml_isf->geom_set_fn);
	this->global_params_fn = xml->getOneNodeValString(xml->rootNode, 
			xml_isf->global_prm_fn);
	this->bound_fn_set = xml->getOneNodeValString(xml->rootNode, 
			xml_isf->bound_fn_set);
	this->res_sat_set_fn = xml->getOneNodeValString(xml->rootNode, 
			xml_isf->res_sat_set);
	this->fracts_sat_set_fn = xml->getOneNodeValString(xml->rootNode, 
			xml_isf->fracts_sat_set);
}


IMStartFile::IMStartFile(){}
IMStartFile::~IMStartFile(){}
