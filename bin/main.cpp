#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>
#include <stdlib.h>
#include <string>
#include <memory>

#include "fluidSetts.h"
#include "makeFluidSet.h"

int main(int argc, char **argv)
{
	std::shared_ptr<FluidSetts> fls (new FluidSetts());

	std::ostringstream oss;
	oss << "/home/marseille/Dropbox/self/otherWork/delika/indicator_mark/";
	oss << "global_programm/files/fluidSet.imf";
	std::string fn = oss.str();

	readFromFileFluidSetts(fn, fls);

	//std::cout << "pow oil = " << fls->powerOil << "\n";

	return 0;
}
