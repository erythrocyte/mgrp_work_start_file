cmake_minimum_required(VERSION 2.8)

set (PROJECT mgrp_work_ofp)

set(MGRP_WORK_OFP_VERSION 0.0.1)

project(${PROJECT})

set(SONAME lmgrp_work_ofp)
set (EXNAME a)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

add_subdirectory(lib)
add_subdirectory(bin)
